import os
import sys


char = "----"


def tree_command(path, inner=0):
    os.chdir(path)
    
    for f in os.listdir():
        os.chdir(path)
        print(char * inner + "|", end=f"{f}\n")

        try:
            temp = path + '\\' + str(f)
            tree_command(temp, inner + 1)
        except OSError:
            pass

    return


def main():
    if len(sys.argv) != 2:
        print("Invalid format!\nFormat: python treeCommand.py <path>")
        sys.exit()

    try:
        os.chdir(sys.argv[1])
    except OSError:
        print(f'Error, Invalid path: {sys.argv[1]}\n')
        sys.exit()

    tree_command(sys.argv[1])


if __name__ == "__main__":
    main()
